package authorize

type AuthorizeRequest struct {
	RuleName  string `json:"ruleName" bson:"ruleName"`
	RuleValue string `json:"ruleValue" bson:"ruleValue"`
}

type AuthorizeBean struct {
	RuleName  string   `json:"ruleName" bson:"ruleName"`
	RuleValue string   `json:"ruleValue" bson:"ruleValue"`
	Roles     []string `json:"roles" bson:"roles"`
}
