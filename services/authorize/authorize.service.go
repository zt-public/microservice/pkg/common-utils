package authorize

import (
	"gitlab.com/zt-public/microservice/pkg/common-utils/logger"
)

type IServiceAuthorize interface {
	QueryAuthorizeByCriteria(l logger.LogModel, params AuthorizeRequest, mogoUrl, dbName, collectionRequest string) (authBean []AuthorizeBean, err error)
}

type serviceAuthorize struct {
	dao IServiceDaoAuthorize
}

func NewServiceAuthorize(dao IServiceDaoAuthorize) IServiceAuthorize {
	return &serviceAuthorize{
		dao: dao,
	}
}

func (service *serviceAuthorize) QueryAuthorizeByCriteria(l logger.LogModel, params AuthorizeRequest, mogoUrl, dbName, collectionRequest string) (authList []AuthorizeBean, err error) {
	authList, err = service.dao.QueryAuthorizeByCriteria(params, l)
	if err != nil {
		return nil, err
	}

	return
}
