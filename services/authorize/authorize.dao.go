package authorize

import (
	"gitlab.com/zt-public/microservice/pkg/common-utils/logger"
	"gitlab.com/zt-public/microservice/pkg/common-utils/utils"
)

type IServiceDaoAuthorize interface {
	QueryAuthorizeByCriteria(params AuthorizeRequest, l logger.LogModel) (authorizeList []AuthorizeBean, err error)
}

type serviceDaoAuthorize struct {
	confDB utils.SystemDatabase
}

func NewIServiceDaoAuthorize(confDB utils.SystemDatabase) IServiceDaoAuthorize {
	return &serviceDaoAuthorize{
		confDB: confDB,
	}
}

func (r *serviceDaoAuthorize) QueryAuthorizeByCriteria(params AuthorizeRequest, l logger.LogModel) (authorizeList []AuthorizeBean, err error) {
	// startStep := time.Now()
	// ctx, cancel := context.WithTimeout(context.Background(), 60*time.Second)
	// defer cancel()

	// reqByte, err := json.Marshal(params)
	// logStepRequest := logger.LogStepRequest{
	// 	StepName:    "DAO- QueryAuthorizeByRole",
	// 	StartDate:   utils.ConvDatetimeFormatLog(startStep),
	// 	StepRequest: string(reqByte),
	// 	Endpoint:    "ZETTA",
	// 	Method:      "DAO - QueryAuthorizeByRole",
	// 	System:      "ZETTA",
	// }

	// client, err := database.GetMongoDB(r.confDB.MongoDB.Uri, r.confDB.MongoDB.DbName, nil)

	// if err != nil {
	// 	logStepRequest.ResultDesc = "Error : GetMongoDB"
	// 	logStepRequest.Response = err.Error()
	// 	logStepRequest.ResultCode = "500"
	// 	logger.LogStep(logStepRequest, l, startStep)
	// 	return
	// }

	// filterList := []bson.D{}
	// if params.RuleName != "" {
	// 	filterList = append(filterList, bson.D{{Key: "ruleName", Value: params.RuleName}})
	// }
	// if params.RuleValue != "" {
	// 	filterList = append(filterList, bson.D{{Key: "ruleValue", Value: params.RuleValue}})
	// }

	// pipeline := []bson.M{
	// 	{"$match": bson.M{"$and": filterList}},
	// 	{
	// 		"$project": bson.M{
	// 			"_id": 0,
	// 		},
	// 	},
	// }

	return
}
