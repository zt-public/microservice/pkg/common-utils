package config

import (
	"context"
	"encoding/json"
	"fmt"
	"time"

	databases "gitlab.com/zt-public/microservice/pkg/common-utils/database"
	"gitlab.com/zt-public/microservice/pkg/common-utils/logger"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var collectionName = "zetta_lov_mst"

const redisTimeout = time.Duration(1) * time.Hour

func QueryCommonConfig(l *logger.LogModel, params GetConfigRequest, mogoUrl, dbName, redisUrl, collectionRequest string) ([]CommonConfigStruct, error) {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}
	var commonStructs []CommonConfigStruct
	var out []byte
	redisKey := "config_" + params.LovType + "_" + params.LovName

	ctx, cancle := context.WithTimeout(context.Background(), 15*time.Minute)
	defer cancle()
	rdb, errRedisConnection := databases.GetRedis(redisUrl, nil)
	if errRedisConnection == nil {
		serviceValue, errRedis := rdb.Get(ctx, redisKey).Result()
		if errRedis == nil {
			json.Unmarshal([]byte(serviceValue), &commonStructs)
			return commonStructs, nil
		}
	}

	database, err := databases.GetMongoDB(mogoUrl, dbName, nil)
	if err != nil {
		return nil, err
	}
	query := []interface{}{}
	if params.LovType != "" {
		query = append(query, bson.M{"lovType": params.LovType})
	}
	if params.LovName != "" {
		query = append(query, bson.M{"lovName": params.LovName})
	}
	if params.LovVal1 != "" {
		query = append(query, bson.M{"lov_val1": params.LovVal1})
	}
	if params.LovVal2 != "" {
		query = append(query, bson.M{"lov_val2": params.LovVal2})
	}
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	cursor, err := database.Collection(collectionName).Find(ctx, bson.M{"$and": query})
	if err != nil {
		return nil, err
	}
	defer cursor.Close(ctx)
	for cursor.Next(ctx) {
		var commonStruct CommonConfigStruct
		cursor.Decode(&commonStruct)
		commonStructs = append(commonStructs, commonStruct)
	}
	if err := cursor.Err(); err != nil {
		return nil, err
	}

	if errRedisConnection == nil {
		out, err = json.Marshal(commonStructs)
		if err != nil {
		} else {
			RedisValue := string(out)
			err = rdb.Set(ctx, redisKey, RedisValue, redisTimeout).Err()
			if err != nil {
				fmt.Println("Cannot set redis key " + redisKey)
			}
		}
	}

	return commonStructs, nil
}

func InsertCommonConfig(l *logger.LogModel, params CommonConfigStruct, mogoUrl, dbName, redisUrl, collectionRequest string) error {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}

	redisKey := "config_" + params.LovType + "_" + params.LovName
	var out []byte
	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	database, err := databases.GetMongoDB(mogoUrl, dbName, nil)
	if err != nil {
		return err
	}
	opts := options.Update().SetUpsert(true)

	filter := bson.M{"lovType": params.LovType}
	var insertValue bson.D

	if params.LovVal1 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val1", Value: params.LovVal1})
	}
	if params.LovVal2 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val2", Value: params.LovVal2})
	}
	if params.LovVal3 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val3", Value: params.LovVal3})
	}
	if params.LovVal4 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val4", Value: params.LovVal4})
	}
	if params.LovVal5 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val5", Value: params.LovVal5})
	}
	if params.LovVal6 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val6", Value: params.LovVal6})
	}
	if params.LovVal7 != "" {
		insertValue = append(insertValue, bson.E{Key: "lov_val7", Value: params.LovVal7})
	}
	insertValue = append(insertValue, bson.E{Key: "lovName", Value: params.LovName})
	insertValue = append(insertValue, bson.E{Key: "createBy", Value: params.CreateBy})
	insertValue = append(insertValue, bson.E{Key: "createDate", Value: time.Now()})
	insertValue = append(insertValue, bson.E{Key: "updateBy", Value: params.CreateBy})
	insertValue = append(insertValue, bson.E{Key: "updateDate", Value: time.Now()})

	update := bson.D{{Key: "$set", Value: insertValue}}
	_, err = database.Collection(collectionName).UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return err
	}

	ctx, cancle := context.WithTimeout(context.Background(), 15*time.Second)
	defer cancle()

	rdb, errRedisConnection := databases.GetRedis(redisUrl, nil)
	if errRedisConnection == nil {
		patternKey := redisKey + "_*"
		matchKeys, errTest := rdb.Keys(ctx, patternKey).Result()
		if errTest == nil {
			if len(matchKeys) > 0 {
				rdb.Del(ctx, matchKeys...)
			}
		}
		var forSave []CommonConfigStruct
		forSave = append(forSave, params)
		out, err = json.Marshal(forSave)
		if err != nil {
			return err
		} else {
			RedisValue := string(out)
			err = rdb.Set(ctx, redisKey, RedisValue, redisTimeout).Err()
			if err != nil {
				fmt.Println("Cannot set redis key " + redisKey)
			}
		}
	}

	return nil
}
