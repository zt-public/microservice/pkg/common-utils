package config

import "time"

type GetConfigRequest struct {
	LovType string `json:"lovType" validate:"required"`
	LovName string `json:"lovName,omitempty" validate:"required"`
	LovVal1 string `json:"lov_val1"`
	LovVal2 string `json:"lov_val2"`
}

type CreateConfigRequest struct {
	LovType    string    `json:"lovType" bson:"lovType" validate:"required"`
	LovName    string    `json:"lovName" bson:"lovName" validate:"required"`
	DisplayVal string    `json:"displayVal" bson:"displayVal"`
	LovVal1    string    `json:"lov_val1" bson:"lov_val1"`
	LovVal2    string    `json:"lov_val2" bson:"lov_val2"`
	LovVal3    string    `json:"lov_val3" bson:"lov_val3"`
	LovVal4    string    `json:"lov_val4" bson:"lov_val4"`
	LovVal5    string    `json:"lov_val5" bson:"lov_val5"`
	LovVal6    string    `json:"lov_val6" bson:"lov_val6"`
	LovVal7    string    `json:"lov_val7" bson:"lov_val7"`
	CreateBy   string    `json:"createBy" bson:"createBy" validate:"required"`
	CreateDate time.Time `json:"createDate" bson:"createDate"`
	UpdateBy   string    `json:"updateBy" bson:"updateBy"`
	UpdateDate string    `json:"updateDate" bson:"updateDate"`
}

type CommonConfigStruct struct {
	LovType    string    `json:"lovType" bson:"lovType"`
	LovName    string    `json:"lovName" bson:"lovName"`
	DisplayVal string    `json:"displayVal" bson:"displayVal"`
	LovVal1    string    `json:"lov_val1" bson:"lov_val1"`
	LovVal2    string    `json:"lov_val2" bson:"lov_val2"`
	LovVal3    string    `json:"lov_val3" bson:"lov_val3"`
	LovVal4    string    `json:"lov_val4" bson:"lov_val4"`
	LovVal5    string    `json:"lov_val5" bson:"lov_val5"`
	LovVal6    string    `json:"lov_val6" bson:"lov_val6"`
	LovVal7    string    `json:"lov_val7" bson:"lov_val7"`
	CreateBy   string    `json:"createBy" bson:"createBy" validate:"required"`
	CreateDate time.Time `json:"createDate" bson:"createDate"`
	UpdateBy   string    `json:"updateBy" bson:"updateBy"`
	UpdateDate string    `json:"updateDate" bson:"updateDate"`
}
