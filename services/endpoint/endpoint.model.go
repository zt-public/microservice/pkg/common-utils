package endpoint

import "time"

type CreateEndpointRequest struct {
	Endpoint string `json:"endpoint" validate:"required"`
	Data     string `json:"data" validate:"required"`
}

type CreateEndpointResponse struct {
	Data string `json:"data"`
}

type EndpointDB struct {
	Endpoint string `json:"endpoint" bson:"endpoint"`
	Data     string `json:"data" bson:"data"`
}
type Endpoint struct {
	Endpoint           string      `json:"endpoint"`
	Url                string      `json:"url"`
	Username           string      `json:"username"`
	Password           string      `json:"password"`
	Timeout            int         `json:"timeout"`
	ParamList          []ParamList `json:"paramList"`
	InsecureSkipVerify bool        `json:"insecureSkipVerify"`
}

type ParamList struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

type GetEndpointRequest struct {
	Endpoint string `json:"endpoint" validate:"required"`
}
type GetEndpointArrayRequest struct {
	Endpoint []string `json:"endpoint" validate:"required"`
}

type ConsentResult struct {
	OrderNo  string      `json:"orderNo"`
	TranId   string      `json:"tranId"`
	Feature  string      `json:"feature"`
	Url      string      `json:"url"`
	Response interface{} `json:"response"`
	Time     time.Time   `json:"time"`
}

type LoadEndpointRequest struct {
	Endpoints          string
	InsecureSkipVerify bool
	Path               []string
	Timeout            int
}
