package endpoint

import (
	"context"
	"fmt"
	"time"

	databases "gitlab.com/zt-public/microservice/pkg/common-utils/database"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var collectionName = "omni_endpoint"

func CreateEndpointDao(endpoint *CreateEndpointRequest, mongoUrl, dbName, collectionRequest string) (response map[string]interface{}, err error) {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}
	fmt.Println("CreateEndpointDao")
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	database, err := databases.GetMongoDB(mongoUrl, dbName, nil)
	fmt.Println(database)
	if err != nil {
		return
	}

	collection := database.Collection(collectionName)
	opts := options.Update().SetUpsert(true)
	filter := bson.D{primitive.E{Key: "endpoint", Value: endpoint.Endpoint}}
	update := bson.D{primitive.E{Key: "$set", Value: bson.D{primitive.E{Key: "data", Value: endpoint.Data}}}}
	updateResult, err := collection.UpdateOne(ctx, filter, update, opts)
	if err != nil {
		return nil, err
	} else {
		return map[string]interface{}{
			"id": updateResult.ModifiedCount,
		}, nil
	}
}

func GetEndpointByEndpointKey(endpoint, mongoUrl, dbName, collectionRequest string) (response EndpointDB, err error) {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()
	database, err := databases.GetMongoDB(mongoUrl, dbName, nil)
	if err != nil {
		return
	}
	collection := database.Collection(collectionName)

	find, err := collection.Find(ctx, bson.M{"endpoint": endpoint})
	if err != nil {
		return response, err
	}

	for find.Next(ctx) {
		err := find.Decode(&response)
		if err != nil {
			return response, err
		}
	}
	defer find.Close(ctx)
	return response, nil
}

func GetEndpointByEndpointKeyArray(endpoint []string, mongoUrl, dbName, collectionRequest string) (response []EndpointDB, err error) {
	if collectionRequest != "" {
		collectionName = collectionRequest
	}
	ctx, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	defer cancel()

	database, err := databases.GetMongoDB(mongoUrl, dbName, nil)
	if err != nil {
		return
	}
	collection := database.Collection(collectionName)

	find, err := collection.Find(ctx, bson.M{"endpoint": bson.M{"$in": endpoint}})
	if err != nil {
		return response, err
	}

	for find.Next(ctx) {
		var ep EndpointDB
		err := find.Decode(&ep)
		if err != nil {
			return response, err
		}
		response = append(response, ep)
	}
	defer find.Close(ctx)
	return response, nil
}
