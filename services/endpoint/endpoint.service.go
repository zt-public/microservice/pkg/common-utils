package endpoint

import (
	"context"
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	b64 "encoding/base64"
	"encoding/json"
	"fmt"
	"html"
	"io"
	"strings"

	databases "gitlab.com/zt-public/microservice/pkg/common-utils/database"
	"gitlab.com/zt-public/microservice/pkg/common-utils/utils"

	"github.com/go-redis/redis"
)

type UnescapedString string

type IServiceEndpoint interface {
	CreateEndpointService(req *CreateEndpointRequest, collectionRequest string) (response CreateEndpointResponse, err error)

	GetEndpointService(req *GetEndpointRequest, collectionRequest string) (response CreateEndpointResponse, err error)

	GetEndpointDetail(endpoint, collectionRequest string) (response Endpoint, err error)

	GetEndpointServiceArray(req *GetEndpointArrayRequest, collectionRequest string) (response []CreateEndpointResponse, err error)
}

type serviceEndpoint struct {
	confDb utils.SystemDatabase
}

func NewServiceEndpoint(confDb utils.SystemDatabase) IServiceEndpoint {
	return &serviceEndpoint{
		confDb: confDb,
	}
}

func (s *UnescapedString) UnmarshalJSON(data []byte) error {
	if err := json.Unmarshal(data, (*string)(s)); err != nil {
		return err
	}
	*s = UnescapedString(html.UnescapeString(string(*s)))
	return nil
}

func (service *serviceEndpoint) CreateEndpointService(req *CreateEndpointRequest, collectionRequest string) (response CreateEndpointResponse, err error) {
	sDec, err := b64.StdEncoding.DecodeString(req.Data)
	if err != nil {
		return response, err
	}

	data1 := Endpoint{}
	d := json.NewDecoder(strings.NewReader(string(sDec)))
	d.DisallowUnknownFields()
	if err := d.Decode(&data1); err != nil {
		return response, err
	}
	key := service.confDb.MongoDB.Encrypt
	result := Encrypt([]byte(key), string(req.Data))

	req.Data = result

	data, err := CreateEndpointDao(req, service.confDb.MongoDB.Uri, service.confDb.MongoDB.DbName, collectionRequest)
	if err != nil {
		return CreateEndpointResponse{}, err
	}
	_ = data
	result2 := Decrypt([]byte(key), result)
	url := service.confDb.Redis.Uri
	rdb, err := databases.GetRedis(url, nil)
	if err == nil {
		set, err := rdb.HSet(context.TODO(), "endpoint", req.Endpoint, result2).Result()
		if err != nil {
			fmt.Printf("set : redis error: %v \n", err)
		}
		fmt.Printf("set : redis set data : %v \n", set)
	}
	response.Data = result2
	return response, nil
}

func (service *serviceEndpoint) GetEndpointService(req *GetEndpointRequest, collectionRequest string) (response CreateEndpointResponse, err error) {
	key := service.confDb.MongoDB.Encrypt
	mongoUrl := service.confDb.MongoDB.Uri
	dbName := service.confDb.MongoDB.DbName
	url := service.confDb.Redis.Uri
	rdb, err := databases.GetRedis(url, nil)
	if err == nil {
		endpointGet, err := rdb.HGet(context.TODO(), "endpoint", req.Endpoint).Result()
		if err != nil && err != redis.Nil {
			fmt.Printf("find: redis error: %v \n", err)

			endpointDb, err := GetEndpointByEndpointKey(req.Endpoint, mongoUrl, dbName, collectionRequest)
			if err != nil {
				return response, err
			}
			if endpointDb.Data == "" {
				return response, nil
			}
			result2 := Decrypt([]byte(key), endpointDb.Data)
			endpointSet, err := rdb.HSet(context.TODO(), "endpoint", endpointDb.Endpoint, result2).Result()
			if err != nil {
				fmt.Printf("set : redis error: %v \n", err)
			}
			fmt.Printf("set : redis set data : %v \n", endpointSet)
			response.Data = result2
			return response, nil
		}

		fmt.Printf("get : redis get data : %v \n", endpointGet)
		response.Data = endpointGet
		return response, err
	}
	endpointDb, err := GetEndpointByEndpointKey(req.Endpoint, mongoUrl, dbName, collectionRequest)
	if err != nil {
		return response, err
	}
	if endpointDb.Data == "" {
		return response, nil
	}
	result2 := Decrypt([]byte(key), endpointDb.Data)
	response.Data = result2
	return response, err
}

func (service *serviceEndpoint) GetEndpointDetail(endpoint, collectionRequest string) (response Endpoint, err error) {
	ep := new(GetEndpointRequest)
	ep.Endpoint = endpoint
	data, err := service.GetEndpointService(ep, collectionRequest)
	if err != nil {
		return
	}
	sDec, err := b64.StdEncoding.DecodeString(data.Data)
	if err != nil {
		return
	}
	fmt.Printf("get Endpoint name %v with data  %v \n", endpoint, string(sDec))
	data1 := Endpoint{}
	if string(sDec) == "" {
		return
	}
	err = json.Unmarshal(sDec, &data1)
	if err != nil {
		return
	}
	response = data1
	return
}

func (service *serviceEndpoint) GetEndpointServiceArray(req *GetEndpointArrayRequest, collectionRequest string) (response []CreateEndpointResponse, err error) {

	mongoUrl := service.confDb.MongoDB.Uri
	dbName := service.confDb.MongoDB.DbName

	key := service.confDb.MongoDB.Encrypt
	endpointDb, err := GetEndpointByEndpointKeyArray(req.Endpoint, mongoUrl, dbName, collectionRequest)
	if err != nil {
		return response, err
	}
	if len(endpointDb) == 0 {
		return response, nil
	}
	for _, vdata := range endpointDb {
		result2 := Decrypt([]byte(key), vdata.Data)
		str := CreateEndpointResponse{}
		str.Data = result2
		response = append(response, str)
	}
	return response, err
}

func (service *serviceEndpoint) GetEndpointDetailArray(endpoint []string, collectionRequest string) (response []Endpoint, err error) {
	ep := new(GetEndpointArrayRequest)
	ep.Endpoint = endpoint
	data, err := service.GetEndpointServiceArray(ep, collectionRequest)
	if err != nil {
		return
	}
	for _, v := range data {
		sDec, err := b64.StdEncoding.DecodeString(v.Data)
		if err != nil {
			return response, err
		}
		fmt.Printf("get Endpoint name %v with data  %v \n", endpoint, string(sDec))
		data1 := Endpoint{}
		if string(sDec) == "" {
			return response, nil
		}
		err = json.Unmarshal(sDec, &data1)
		if err != nil {
			return response, err
		}
		response = append(response, data1)
	}
	return response, nil

}

func Encrypt(key []byte, text string) string {
	plaintext := []byte(text)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	ciphertext := make([]byte, aes.BlockSize+len(plaintext))
	iv := ciphertext[:aes.BlockSize]
	if _, err := io.ReadFull(rand.Reader, iv); err != nil {
		panic(err)
	}

	stream := cipher.NewCFBEncrypter(block, iv)
	stream.XORKeyStream(ciphertext[aes.BlockSize:], plaintext)

	return b64.URLEncoding.EncodeToString(ciphertext)
}
func Decrypt(key []byte, cryptoText string) string {
	ciphertext, _ := b64.URLEncoding.DecodeString(cryptoText)

	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}

	if len(ciphertext) < aes.BlockSize {
		panic("ciphertext too short")
	}
	iv := ciphertext[:aes.BlockSize]
	ciphertext = ciphertext[aes.BlockSize:]

	stream := cipher.NewCFBDecrypter(block, iv)

	stream.XORKeyStream(ciphertext, ciphertext)

	return string(ciphertext)
}
