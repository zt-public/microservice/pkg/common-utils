package database

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/denisenkom/go-mssqldb"
	mssql "github.com/denisenkom/go-mssqldb"
	"github.com/denisenkom/go-mssqldb/msdsn"
)

var sqlServerDb = make(map[string]*sql.DB)

func InitialSQLServerDatabase(uri string) (err error) {
	fmt.Println("New MSSQL Connection")

	_, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	cfg, _, _ := msdsn.Parse(uri)
	db := mssql.NewConnectorConfig(cfg)
	conn := sql.OpenDB(db)
	conn.SetMaxOpenConns(2)
	conn.SetMaxIdleConns(5)
	conn.SetConnMaxLifetime(time.Second * 120)
	conn.SetConnMaxIdleTime(time.Second * 120)
	if err = conn.Ping(); err != nil {
		defer cancel()
		return
	}
	defer cancel()
	sqlServerDb[uri] = conn
	return
}

func GetMsClient(uri string) (db *sql.DB, err error) {
	if value, found := sqlServerDb[uri]; !found || sqlServerDb[uri] == nil {
		err = InitialSQLServerDatabase(uri)
		if err != nil {
			return
		}
	} else {
		db = value
		return
	}
	db = sqlServerDb[uri]
	return
}
