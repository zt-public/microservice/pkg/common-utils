package database

import (
	"context"
	"fmt"
	"time"

	"github.com/go-redis/redis/v8"
)

var rdb = make(map[string]*redis.Client)

func InitRedis(uri string, option *redis.Options) (err error) {
	opsRedis := redis.Options{}
	if option != nil {
		opsRedis = *option
	}
	fmt.Println("New Redis Connection")
	if option == nil {
		opsRedis = redis.Options{
			Password:     "",
			DB:           0,
			PoolSize:     150,
			MinIdleConns: 50,
		}
	}
	opsRedis.Addr = uri
	rdb[uri] = redis.NewClient(&opsRedis)
	redisContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_, err = rdb[uri].Ping(redisContext).Result()
	if err != nil {
		return
	}
	return
}

func GetRedis(uri string, option *redis.Options) (rd *redis.Client, err error) {
	if value, found := rdb[uri]; !found || rdb[uri] == nil {
		err = InitRedis(uri, option)
		if err != nil {
			return
		}
	} else {
		rd = value
		return
	}
	rd = rdb[uri]
	return
}

func GetRedisPool(uri string) *redis.Client {
	return rdb[uri]
}
