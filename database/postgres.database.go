package database

import (
	"context"
	"database/sql"
	"fmt"
	"time"

	_ "github.com/lib/pq"
)

var postgresDB = make(map[string]*sql.DB)

func InitialPostgreSQLDatabase(uri string) (err error) {
	fmt.Println("New PostgreSQL Connection")

	_, cancel := context.WithTimeout(context.Background(), 30*time.Second)
	db, err := sql.Open("postgres", uri)
	if err != nil {
		defer cancel()
		return err
	}

	db.SetMaxOpenConns(2)
	db.SetMaxIdleConns(5)
	db.SetConnMaxLifetime(time.Second * 120)
	db.SetConnMaxIdleTime(time.Second * 120)

	if err = db.Ping(); err != nil {
		defer cancel()
		return err
	}

	defer cancel()
	postgresDB[uri] = db
	return nil
}

func GetPostgresClient(uri string) (db *sql.DB, err error) {
	if value, found := postgresDB[uri]; !found || postgresDB[uri] == nil {
		err = InitialPostgreSQLDatabase(uri)
		if err != nil {
			return nil, err
		}
	} else {
		db = value
		return db, nil
	}
	db = postgresDB[uri]
	return db, nil
}
