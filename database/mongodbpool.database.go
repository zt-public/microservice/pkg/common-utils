package database

import (
	"context"
	"fmt"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
)

var client = make(map[string]*mongo.Client)
var mongoDB = make(map[string]*mongo.Database)

func InitMongoDB(uri, dbName string, option *options.ClientOptions) (err error) {
	fmt.Println("New Mongo Connection")

	if option == nil {
		option = options.Client()
		option.SetMaxPoolSize(10)
		option.SetMinPoolSize(5)
		option.SetMaxConnecting(2)
		option.SetMaxConnIdleTime(time.Second * 5)
	}
	option.ApplyURI(uri)
	if client[uri], err = mongo.NewClient(option); err != nil {
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	err = client[uri].Connect(ctx)
	if err != nil {
		cancel()
		return
	}
	defer cancel()

	err = client[uri].Ping(context.Background(), readpref.Secondary())
	if err != nil {
		return
	}
	mongoDB[uri] = client[uri].Database(dbName)
	return
}

func GetMongoDB(uri, dbName string, option *options.ClientOptions) (db *mongo.Database, err error) {
	if value, found := mongoDB[uri]; !found || mongoDB[uri] == nil {
		err = InitMongoDB(uri, dbName, option)
		if err != nil {
			return
		}
	} else {
		db = value
		return
	}
	db = mongoDB[uri]
	return
}

func MongoGetClientFromDBConnect(uri string) *mongo.Client {
	return client[uri]
}

func GetMongoDbPool(uri string) *mongo.Database {
	return mongoDB[uri]
}

func MongoPoolCheckHealth(ctx context.Context, uri string) (err error) {
	err = client[uri].Ping(ctx, readpref.Secondary())
	return
}
