package logger

import (
	"fmt"
	"os"
	"time"

	utils "gitlab.com/zt-public/microservice/pkg/common-utils/utils"

	"github.com/elastic/go-elasticsearch/v8"
	"github.com/sirupsen/logrus"
	logs "github.com/sirupsen/logrus"
	"gopkg.in/go-extras/elogrus.v8"
)

var log = logs.New()

func init() {
	log.SetFormatter(&logs.JSONFormatter{})
	log.SetOutput(os.Stdout)
}

func InitialElasticLog(Addresses string, username string, password string, certificateFingerPrint string, suffix string, host string) {
	client, err := elasticsearch.NewClient(elasticsearch.Config{
		Addresses:              []string{Addresses},
		Username:               username,
		Password:               password,
		CertificateFingerprint: certificateFingerPrint,
	})
	if err != nil {
		log.Panic(err)
	}
	hook, err := elogrus.NewAsyncElasticHook(client, host, logrus.InfoLevel, suffix)
	if err != nil {
		log.Panic(err)
	}
	log.Hooks.Add(hook)
}

func (Log *LogModel) LogOrderSuccess() {
	fmt.Println("--- LogOrderSuccess ---")
	Log.StepTxid = Log.Txid
	Log.LogCat = "order"
	Log.ResultIndicator = "COMPLETED"
	Log.EndDate = utils.ConvDatetimeFormatLog(time.Now())
	m := MapStruct(Log)
	log.WithFields(m).Info(Log.ResultIndicator)
}

func (Log *LogModel) LogOrderFail() {
	fmt.Println("--- LogOrderFail ---")
	Log.StepTxid = Log.Txid
	Log.LogCat = "order"
	Log.ResultIndicator = "FAILED"
	Log.EndDate = utils.ConvDatetimeFormatLog(time.Now())
	m := MapStruct(Log)
	log.WithFields(m).Info(Log.ResultIndicator)
}

func (Log *LogModel) LogStepSuccess() {
	fmt.Println("--- LogStepSuccess ---")
	Log.StepTxid = Log.Txid
	Log.LogCat = "detail"
	Log.ResultIndicator = "SUCCESS"
	Log.EndDate = utils.ConvDatetimeFormatLog(time.Now())
	m := MapStruct(Log)
	log.WithFields(m).Info(Log.ResultIndicator)
}

func (Log *LogModel) LogStepFail() {
	fmt.Println("--- LogStepFail ---")
	Log.StepTxid = Log.Txid
	Log.LogCat = "detail"
	Log.ResultIndicator = "FAILED"
	Log.EndDate = utils.ConvDatetimeFormatLog(time.Now())
	m := MapStruct(Log)
	log.WithFields(m).Info(Log.ResultIndicator)
}

func LogInfo(s string) {
	fmt.Println(s)
}
