package utils

import (
	"time"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/limiter"
	"github.com/gofiber/fiber/v2/middleware/requestid"
	jwtware "github.com/gofiber/jwt/v2"
	"github.com/golang-jwt/jwt/v5"
)

type Middleware struct{}

type IMiddleware interface {
	AuthorizationRequired(jwtSecret string) fiber.Handler
	InitialMiddlewareRequestId(header string) (iRequestId fiber.Handler)
	InitialLimiter(numOfLimit int) (iLimiter fiber.Handler)
	CreateToken(data map[string]interface{}, secret string, hourExpired int) (token string, err error)
	RestrictedToken(tokenString string, secret string) (claims jwt.MapClaims, err error)
}

func NewMiddleware() IMiddleware {
	return &Middleware{}
}

func (r *Middleware) InitialMiddlewareRequestId(header string) (iRequestId fiber.Handler) {
	if header != "" {
		iRequestId = requestid.New(requestid.Config{
			Header: header,
			Generator: func() string {
				return GetUUID()
			},
		})
	} else {
		iRequestId = requestid.New()
	}
	return
}

func (r *Middleware) InitialLimiter(numOfLimit int) (iLimiter fiber.Handler) {
	if numOfLimit == 0 {
		numOfLimit = 20
	}
	iLimiter = limiter.New(limiter.Config{
		Next: func(c *fiber.Ctx) bool {
			return c.IP() == "127.0.0.1"
		},
		Max:        numOfLimit,
		Expiration: 30 * time.Second,
		KeyGenerator: func(c *fiber.Ctx) string {
			return c.Get("x-forwarded-for")
		},
		LimitReached: func(c *fiber.Ctx) error {
			return c.Status(fiber.StatusInternalServerError).JSON(fiber.Map{"message": "Rate Limit Too Many Requests"})
		},
	})
	return
}

func (r *Middleware) AuthorizationRequired(jwtSecret string) fiber.Handler {
	return jwtware.New(jwtware.Config{
		SuccessHandler: authSuccess,
		ErrorHandler:   authError,
		SigningKey:     []byte(jwtSecret),
		SigningMethod:  "HS256",
	})
}

func authSuccess(c *fiber.Ctx) error {
	c.Next()
	return nil
}

func authError(c *fiber.Ctx, e error) error {
	c.Status(fiber.StatusUnauthorized).JSON(fiber.Map{
		"error": "Unauthorized",
		"msg":   e.Error(),
	})
	return nil
}

func (r *Middleware) CreateToken(data map[string]interface{}, secret string, hourExpired int) (token string, err error) {
	claims := jwt.MapClaims{}
	claims["iat"] = time.Now().Unix()
	claims["exp"] = time.Now().Add(time.Hour * time.Duration(hourExpired)).Unix()
	for key, value := range data {
		claims[key] = value
	}
	t := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err = t.SignedString([]byte(secret))
	return
}

func (r *Middleware) RestrictedToken(tokenString string, secret string) (claims jwt.MapClaims, err error) {
	claims = jwt.MapClaims{}
	_, err = jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return []byte(secret), nil
	})

	if err != nil {
		return
	}

	return
}
